import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Button from "../components/Button";
import Input from "../components/Input";
import SocialMedia from "../components/SocialMedia";

const Login = () => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const navigate = useNavigate();
  console.log(password);

  const handleLogin = () => {
    const accessToken = username;
    localStorage.setItem("accessToken", accessToken);
    navigate("/dashboard");
  };

  return (
    <div className="authentication-container">
      <div className="form-container">
        <form className="auth-form">
          <h2 className="title" style={{ marginBottom: "40px" }}>
            Sign In
          </h2>
          <Input
            type="text"
            placeholder="username"
            handleClick={(event) => {
              setUsername(event.target.value);
            }}
          />
          <Input
            type="password"
            placeholder="password"
            handleClick={(event) => {
              setPassword(event.target.value);
            }}
          />
          <Link to={"/dashboard"} style={{ textDecoration: "none" }}>
            <Button margin="20px" title="login" handleClick={handleLogin} />
          </Link>
          <SocialMedia text="Sign In" />
        </form>
        <div className="bottom-link">
          <Link to={"/register"}>
            <p>new here?</p>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Login;
