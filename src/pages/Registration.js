import { useState } from "react";
import { Link } from "react-router-dom";
import Button from "../components/Button";
import Input from "../components/Input";
import SocialMedia from "../components/SocialMedia";

const Registration = () => {
  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  return (
    <div className="authentication-container">
      <div className="form-container">
        <form className="auth-form">
          <h2 className="title">Sign Up</h2>
          <Input
            type="text"
            placeholder="username"
            handleClick={(event) => {
              setUsername(event.target.value);
            }}
          />
          <Input
            type="email"
            placeholder="email"
            handleClick={(event) => {
              setEmail(event.target.value);
            }}
          />
          <Input
            type="password"
            placeholder="password"
            handleClick={(event) => {
              setPassword(event.target.value);
            }}
          />
          <Link to={"/login"} style={{textDecoration: "none"}}>  
          <Button
            margin="5px"
            title="Register"
            handleClick={() => {
              console.log(`username: ${username}`);
              console.log(`email: ${email}`);
              console.log(`password: ${password}`);
              console.log(`send to backend`);
            }}
          />
          </Link>
          <SocialMedia text="Sign Up" />
        </form>
        <div className="bottom-link">
          <Link to={"/login"}>
            <p>already have an account?</p>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Registration;
