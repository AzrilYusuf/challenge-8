import { useState } from "react";
import { Link } from "react-router-dom";
import Button from "../components/Button";
import RoomBox from "../components/RoomBox";
import Biodata from "../components/Biodata";
import ProfilePicture from "../components/ProfilePicture";

const Dashboard = () => {
  const [rooms] = useState([
    {
      title: "Game one",
      player1: "Azril",
      player1_choice: "Rock",
      player2: "Yusuf",
      player2_choice: "Scissors",
    },
    {
      title: "Game two",
      player1: "Hadi",
      player1_choice: "Paper",
      player2: "Zayn",
      player2_choice: "Scissors",
    },
    {
      title: "Game three",
      player1: "Dewa",
      player1_choice: "Rock",
      player2: "",
      player2_choice: "",
    },
    {
      title: "Room four",
      player1: "Harry",
      player1_choice: "Scissors",
      player2: "Ron",
      player2_choice: "Rock",
    },
    {
      title: "Room five",
      player1: "Dewa",
      player1_choice: "Rock",
      player2: "Gilang",
      player2_choice: "Scissors",
    },
    {
      title: "Room six",
      player1: "Budi",
      player1_choice: "paper",
      player2: "",
      player2_choice: "",
    },
  ]);

  return (
    <div className="dashboard">
      <div className="game">
        <div className="upper-game">
          <Link to={''} className="single-player-box"style={{textDecoration: "none"}}>
            <div className="single-player-title">PLAY VS COM</div>
          </Link>
          <div className="create-room">
            <Link to={"/create-room"} style={{textDecoration: "none"}}>
              <Button title="Create Room" />
            </Link>
          </div>
        </div>
        <hr style={{border: "1px solid", color:"grey", backgroundColor: "white", width:"100%"}} />
        <Link to={''} className="rooms" style={{textDecoration: "none"}}>
          {rooms.map((room) => {
            return (
              <RoomBox
                title={room.title}
                player1={room.player1}
                player1_choice={room.player1_choice}
                player2={room.player2}
                player2_choice={room.player2_choice}
              />
            );
          })}
        </Link>
      </div>
      <div className="biodata">
        <div>
          <ProfilePicture />
        </div>
        <div>
          <Biodata />
        </div>
        <Link style={{textDecoration: "none"}}>
          <Button title="game history" />
        </Link>
      </div>
    </div>
  );
};

export default Dashboard;
