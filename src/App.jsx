import "./App.css";
import { Routes, Route, Navigate } from "react-router-dom";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import Dashboard from "./pages/Dashboard";
import CreateRoom from "./pages/CreateRoom";

const App = () => {
  const ProtectedRoute = ({ children }) => {
    const accessToken = localStorage.getItem("accessToken");
    if (!accessToken) {
      return <Navigate to={"/login"} />;
    } else {
      return children;
    }
  };

  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/register" element={<Registration />} />
      <Route
        path="/dashboard"
        element={
          <ProtectedRoute>
            <Dashboard />
          </ProtectedRoute>
        }
      />
      <Route
        path="/create-room"
        element={
          <ProtectedRoute>
            <CreateRoom />
          </ProtectedRoute>
        }
      />
      <Route path="*" element={<center>404 page is not found</center>} />
    </Routes>
  );
};

export default App;
