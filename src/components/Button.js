import "../App.css";

const Button = (props) => {
  let buttonTitle = props.title ?? "Button";

  return (
    <button 
    className="button"
    style={{margin: props.margin}}
    onClick={props.handleClick}
    >
        {buttonTitle}
    </button>
);
};

export default Button;
