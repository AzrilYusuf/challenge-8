import picture from '../assets/default-profile-picture.webp'

const ProfilePicture = () => {
  return (
    <div>
        <img src={picture} alt="default-profile-picuture" width="70px" height="70px" style={{borderRadius: "50%", borderStyle: 'solid'}}/>
    </div>
  )
}

export default ProfilePicture