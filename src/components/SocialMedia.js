import "../App.css";

const SocialMedia = (props) => {
  let signText = props.text ?? "Submit";

  return (
    <div
      style={{ marginTop: props.marginLogin }}
      className="container-social-media"
    >
      <p className="social-text">Or {signText} with social platforms</p>
      <div className="social-media">
        <a href="https://google.com" className="social-icon">
          <i className="fa fa-google"></i>
        </a>
        <a href="https://facebook.com" className="social-icon">
          <i className="fa fa-facebook"></i>
        </a>
      </div>
    </div>
  );
};

export default SocialMedia;
