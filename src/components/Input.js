import "../App.css";

const Input = (props) => {
  let inputType = props.type ?? "Text";
  let inputPlaceholder = props.placeholder ?? "enter text here";

  return (
    <div className="input-field">
      <input
        type={inputType}
        placeholder={inputPlaceholder}
        onChange={props.handleClick}
      />
    </div>
  );
};

export default Input;
