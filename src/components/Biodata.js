const Biodata = () => {
  let username = "Wizardian";
  let email = "harry53@gmail.com";
  let fullname = "Harry Potter";
  let phone = "0864788792";
  let address = "London, UK";
  let birth = "31 July 1980";

  return (
    <div
      className="container-bio"
      style={{ fontSize: "large" }}
    >
      <tr>
        <th style={{ width: "10rem" }}></th>
        <th style={{ width: "0.5rem" }}></th>
        <th style={{ width: "15rem" }}></th>
      </tr>
      <tr>
        <td>Username</td>
        <td>:</td>
        <td>{username}</td>
      </tr>
      <tr>
        <td>Email</td>
        <td>:</td>
        <td>{email}</td>
      </tr>
      <tr>
        <td>Fullname</td>
        <td>:</td>
        <td>{fullname} </td>
      </tr>
      <tr>
        <td>Phone</td>
        <td>:</td>
        <td>{phone}</td>
      </tr>
      <tr>
        <td>Address</td>
        <td>:</td>
        <td>{address}</td>
      </tr>
      <tr>
        <td>Birth Date</td>
        <td>:</td>
        <td>{birth}</td>
      </tr>
    </div>
  );
};

export default Biodata;
