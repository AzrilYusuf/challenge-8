import "../App.css";

const RoomBox = (props) => {
  let title = props.title;
  let player1 = props.player1;
  let player1_choice = props.player1_choice;
  let player2 = props.player2;
  let player2_choice = props.player2_choice;

  return (
    <div className="room-box">
      <div className="title-room">{title}</div>
      <div className="content-room">
        <div>
          <p>Player 1 : {player1}</p>
          <p>Choice P1 : {player1_choice}</p>
        </div>
        <div>
          <p>Player 2 : {player2}</p>
          <p>Choice P2 : {player2_choice}</p>
        </div>
      </div>
    </div>
  );
};

export default RoomBox;
